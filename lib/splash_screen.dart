import 'package:hexcolor/hexcolor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';

import 'begin_screen.dart';
Color splash_screen_color = Color(0xFFff5500);
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Stack(
            children: <Widget>[
              SplashScreen(
                seconds: 2,
                navigateAfterSeconds:new BeginScreen(),
                photoSize: 100.0,
                backgroundColor: splash_screen_color,
                styleTextUnderTheLoader: new TextStyle(),
                loaderColor: splash_screen_color,
        ),
              Align(
                alignment: Alignment.center,
                child: Image(
                  image: AssetImage('assets/soundcloudicon.png'),
                  fit: BoxFit.cover,
                  height: 160,
                  width: 160,
                ),
              ),
    ],
        ),
    );
  }
}


