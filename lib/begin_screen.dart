import 'package:flutter/material.dart';
import 'create_account.dart';
import 'login_account.dart';
Color splash_screen_color = Color(0xFFff5500);
class BeginScreen extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
          decoration: BoxDecoration(
              image: DecorationImage(
               image: AssetImage("assets/bilie_begin.jpg"),
          fit: BoxFit.cover,
        ),
        )),
          Align(
            alignment: Alignment.topCenter,
            child: Image(
              image: AssetImage("assets/sclogo.png"),
              fit: BoxFit.cover,
              height: MediaQuery.of(context).size.height * 0.2,
              width: MediaQuery.of(context).size.width * 0.2,
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: MediaQuery.of(context).size.height * 0.35,
              width: MediaQuery.of(context).size.width * 0.9,
              child: new Column(
                children: <Widget>[
                  Text(
                      "Find music you love.",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 30,
                        fontWeight: FontWeight.w500,
                        fontFamily: 'Abandon',
                        letterSpacing: 1.0,
                )),
                  Text(
                      "Discover new artists.",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 30,
                        fontWeight: FontWeight.w500,
                        fontFamily: 'Abandon',
                        letterSpacing: 1.0,
                      )),
                  TextButton(
                          onPressed: () {  },
                          child: CreatAccountBtn()
                      ),
                  TextButton(
                          onPressed: () {  },
                          child: LogInBtn()
                  ),
                ],
              ),

            ),
          )
        ],
      ),
    );
  }
}
class CreatAccountBtn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(child: Padding(
      padding: EdgeInsets.only(top: 20),
      child: SizedBox (
        width: 550,
        height: 40,
        child: TextButton(
            style: TextButton.styleFrom(
              shadowColor: Colors.black,
              primary: Colors.white,
              backgroundColor: splash_screen_color,
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => CreateAccountUI()),
              );
            },
            child: Text('Create an account')) ,
      ),));
  }
}

class LogInBtn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(child: Padding(
      padding: EdgeInsets.only(top: 1),
      child: SizedBox (
      width: 550,
      height: 40,
      child: TextButton(
          style: TextButton.styleFrom(
            shadowColor: Colors.black,
            primary: Colors.black,
            backgroundColor: Colors.white,
          ),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => LoginUI()),
            );
          },
          child: Text('I already have an account',)) ,
    ),));
  }
}