import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class DetailScreen extends StatefulWidget {
  const DetailScreen({Key? key}) : super(key: key);

  @override
  State<DetailScreen> createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: Icon(Icons.arrow_back),
      ),
      extendBodyBehindAppBar: true,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
              height: MediaQuery.of(context).size.height/2,
              child: 
              Stack(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 40.0),

                      
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15.0),
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: NetworkImage('https://image.thanhnien.vn/w660/Uploaded/2022/znetns/2021_03_26/725a8593_kmyk.jpg'),)
                        ),
                      ),
                    
                  ),
             
                  
                ],
              ),
              ),
              Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 5.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        
                        Text('Khong con cua nhau ./ Mad L ft Bin',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20),),
                         SizedBox(height: 5,),
                        Text('Track station.50 Track .31241. Updated just now!!',
                        style: TextStyle(fontSize: 10),),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          
                          children: [
                            
                            Padding(
                              padding: const EdgeInsets.all(8),

                              child: Material(
                                  
                                 elevation: 5,
                                 borderRadius: BorderRadius.circular(30),

                                color: Colors.black,
                                
                                child: MaterialButton(
                                  minWidth: MediaQuery.of(context).size.width/3,
                                  onPressed: (){},
                                  child: Text('Play',
                                  style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white),
                                  textAlign: TextAlign.center,), ),
                              ),),
                              Padding(
                              padding: const EdgeInsets.all(8),

                              child: Material(
                                
                                 elevation: 5,
                                 borderRadius: BorderRadius.circular(30),

                                color: Colors.black,
                                
                                child: MaterialButton(
                                  minWidth: MediaQuery.of(context).size.width/3,
                                  onPressed: (){},
                                  child: Text('Shuffle',
                                  style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white),
                                  textAlign: TextAlign.center,), ),
                              ),)
                          ],
                        )  
                      ],
                    ),
                  ),
                 
      ]),
    );
  }
}