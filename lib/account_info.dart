import 'package:cupertino_icons/cupertino_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:mysoundcloud/begin_screen.dart';
import 'package:mysoundcloud/login_account.dart';


class AccountInfo extends StatefulWidget {
  const AccountInfo({Key? key}) : super(key: key);

  @override
  State<AccountInfo> createState() => _LibraryScreenState();
}

class _LibraryScreenState extends State<AccountInfo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          // automaticallyImplyLeading: false,
          backgroundColor: Colors.black,
          elevation: 0,
          actions: [
            // Padding(
            //   padding: const EdgeInsets.fromLTRB(0, 10, 20, 10),
            //   child: GestureDetector(
            //     onTap: () {
            //
            //     },
            //     child: CircleAvatar(
            //       backgroundImage: NetworkImage('https://lh3.googleusercontent.com/a-/AOh14GgRk0YcMRytT_omkonSktONQd1VsSV0XVQ00Otr6w=s83-c-mo'),
            //     ),
            //   ),
            //
            // )
          ]),
      body: ListView(
          children: [
            Container(
                color: Colors.black,
                height: 650,
                child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5.0),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'More',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 28, color: Colors.white),),
                          SizedBox(
                            height: 35,
                          ),
                          Row (
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(

                                children: [
                                  CircleAvatar(
                                    backgroundImage: NetworkImage('https://lh3.googleusercontent.com/a-/AOh14GgRk0YcMRytT_omkonSktONQd1VsSV0XVQ00Otr6w=s83-c-mo'),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Nguyễn Minh Trí',
                                        style: TextStyle(
                                            fontSize: 12,
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold
                                        ),
                                      ),
                                      SizedBox(
                                        height: 3,
                                      ),
                                      Text(
                                        'View public profile',
                                        style: TextStyle(
                                            fontSize: 14,
                                            color: Color(0xFF999999),
                                            fontWeight: FontWeight.w500
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),

                              Icon(
                                  Icons.mode_edit,
                                   color: Colors.white,
                              ),
                            ],
                          ),

                          Padding(
                            padding: EdgeInsets.only(top: 30),
                            child: SizedBox(
                              height: 15,),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Record',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18,
                                ),
                              ),

                              Icon(
                                CupertinoIcons.chevron_right,
                                color: Colors.white,
                                size: 15,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 28,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Settings',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18,
                                ),
                              ),

                              Icon(
                                CupertinoIcons.chevron_right,
                                color: Colors.white,
                                size: 15,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 50,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Support',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18,
                                ),
                              ),

                              Icon(
                                CupertinoIcons.chevron_right,
                                color: Colors.white,
                                size: 15,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 28,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Legal',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18,
                                ),
                              ),

                              Icon(
                                CupertinoIcons.chevron_right,
                                color: Colors.white,
                                size: 15,
                              ),
                            ],
                          ),


                          SizedBox(
                            height: 50,
                          ),

                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) =>  BeginScreen()),
                              );
                              },
                            child: Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Sign out',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                    ),
                                  ),

                                  Icon(
                                    CupertinoIcons.chevron_right,
                                    color: Colors.white,
                                    size: 15,
                                  ),
                                ],
                              ),
                            ),
                          ),

                          SizedBox(
                            height: 50,
                          ),

                          Align(
                            alignment: Alignment.center,
                            child: Text('App version 2022.04.25-release (125080)',
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.white,
                              ),),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: Text('Flipper version 6.0.5',
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.white,
                              ),),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: Text('Troubleshooting id',
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.white,
                              ),),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: Text('8096f83d-d447-47b0-a5c5-0960e5609630',
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.white,
                              ),),
                          ),



                        ]
                    )
                )
            )
          ]

      ),

    );



  }
}