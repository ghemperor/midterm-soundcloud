import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cupertino_icons/cupertino_icons.dart';
import 'package:flutter/services.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:email_validator/email_validator.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mysoundcloud/home_screen.dart';
import 'package:mysoundcloud/model/user_model.dart';
import 'package:mysoundcloud/root_screen.dart';
Color btn_back = Color(0xFF303030);
Color crtacc = Color(0xFFdedede);
Color google = Color(0xFF303030);

class CreateAccountUI extends StatefulWidget {
  const CreateAccountUI({Key? key}) : super(key: key);

  @override
  State<CreateAccountUI> createState() => _CreateAccountUIState();
}

class _CreateAccountUIState extends State<CreateAccountUI> {
  final _auth = FirebaseAuth.instance;

  final _formKey = GlobalKey<FormState>();

  final TextEditingController emailController = new TextEditingController();
  final TextEditingController passwordController = new TextEditingController();


  @override
  Widget build(BuildContext context) {
    final emailField = TextFormField(
      style: TextStyle(
          color: Color(0xFF979797)
      ),
      decoration: const InputDecoration(

        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Color(0xFF979797)),
        ),
        hintText: "Email",
        hintStyle: TextStyle(
            color: Color(0xFF979797)
        ),
      ),
      autofocus: false,
      controller: emailController,
      keyboardType: TextInputType.emailAddress,
      onSaved: (value)
      {
        emailController.text = value!;
      },
      textInputAction: TextInputAction.next,
    );
    final passwordField = TextFormField(
      obscureText: true,
      style: TextStyle(
          color: Color(0xFF979797)
      ),
      decoration: const InputDecoration(
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Color(0xFF979797)),
        ),
        hintText: "Password (min. 8 characters)",
        hintStyle: TextStyle(
            color: Color(0xFF979797)
        ),
      ),
      autofocus: false,
      controller: passwordController,
      onSaved: (value)
      {
        passwordController.text = value!;
      },
      textInputAction: TextInputAction.done,
    );

    final loginButton = TextButton(
        style: TextButton.styleFrom(
            shadowColor: Colors.black,
            primary: Colors.black,
            backgroundColor: Color(0xFF666666)
        ),
        onPressed: () {
          signUp(emailController.text, passwordController.text);
        },
        child: Text('Sign up with email'));
    return Container(
      child: GestureDetector(
          onTap: () {
            FocusManager.instance.primaryFocus?.unfocus();
          },
          child: Scaffold(
            backgroundColor: Colors.black,
            body: ListView(
              children:[
                Column(
                children: <Widget>[
                  Align(
                    alignment: Alignment.topCenter,
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(7, 30, 0, 0),
                      child: Row(
                        children: <Widget>[
                          ElevatedButton(
                            onPressed: () {
                              Navigator.pop(context);

                            },
                            child: Icon(CupertinoIcons.arrow_left, color: Colors.white),
                            style: ElevatedButton.styleFrom(
                              shape: CircleBorder(),
                              primary: btn_back,
                              fixedSize: Size(40, 40),
                            ),
                          ),
                          Text(
                            " Create account",
                            style: TextStyle(
                              color: crtacc,
                              fontSize: 25,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 0.4,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 30),
                    child: SizedBox(
                      width: 370,
                      height: 45,
                      child:  SignInButton(
                        Buttons.Google,
                        text: "Sign up with Google",
                        onPressed: () {},
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20),
                    child: SizedBox(
                      width: 370,
                      height: 45,
                      child:  SignInButton(
                        Buttons.Facebook,
                        text: "Sign up with Facebook",
                        onPressed: () {},
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20),
                    child: SizedBox(
                      width: 370,
                      height: 45,
                      child:  SignInButton(
                        Buttons.Apple,
                        text: "Sign up with Apple",
                        onPressed: () {},
                      ),
                    ),
                  ),
                  Container(
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(top: 20),
                            child: SizedBox(
                              width: MediaQuery.of(context).size.width * 0.9,
                              height: MediaQuery.of(context).size.height * 0.07,
                              child: emailField,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 10),
                            child: SizedBox(
                              width: MediaQuery.of(context).size.width * 0.9,
                              height: MediaQuery.of(context).size.height * 0.07,
                              child: passwordField,
                            ),
                          ),

                        ],
                      ),

                    ),
                  ),
                  // Padding(
                  //     padding: EdgeInsets.only(top: 20),
                  //     child: SizedBox(
                  //         width: MediaQuery.of(context).size.width * 0.9,
                  //         height: MediaQuery.of(context).size.height * 0.07,
                  //         child:  TextFormField(
                  //           validator: (value) => EmailValidator.validate(value) ? null : "Please enter a valid email",
                  //           style: TextStyle(
                  //               color: Color(0xFF979797)
                  //           ),
                  //           decoration: const InputDecoration(
                  //
                  //             enabledBorder: UnderlineInputBorder(
                  //               borderSide: BorderSide(color: Color(0xFF979797)),
                  //             ),
                  //             hintText: "Email",
                  //             hintStyle: TextStyle(
                  //                 color: Color(0xFF979797)
                  //             ),
                  //
                  //           ),
                  //         )
                  //     )
                  // ),
                  // Padding(
                  //     padding: EdgeInsets.only(top: 20),
                  //     child: SizedBox(
                  //         width: MediaQuery.of(context).size.width * 0.9,
                  //         height: MediaQuery.of(context).size.height * 0.07,
                  //         child: TextFormField(
                  //           validator: (value) {
                  //             if(value==null) {
                  //               return "Enter your password";
                  //             }
                  //             return null;
                  //           },
                  //           obscureText: true,
                  //           style: TextStyle(
                  //               color: Color(0xFF979797)
                  //           ),
                  //           decoration: const InputDecoration(
                  //             enabledBorder: UnderlineInputBorder(
                  //               borderSide: BorderSide(color: Color(0xFF979797)),
                  //             ),
                  //             hintText: "Password (min. 8 characters)",
                  //             hintStyle: TextStyle(
                  //                 color: Color(0xFF979797)
                  //             ),
                  //           ),
                  //         )
                  //     )
                  // ),
                  Padding(
                    padding: EdgeInsets.only(top: 20),
                    child: SizedBox (
                      width: MediaQuery.of(context).size.width * 0.9,
                      height: MediaQuery.of(context).size.height * 0.07,
                      child: loginButton
                    ),)
                ],
              ),
            ]),
          )),
    );
  }
  void signUp(String email, String password) async {
    if(_formKey.currentState!.validate()) {
      await _auth.createUserWithEmailAndPassword(email: email, password: password)
          .then((value) => {
            postDetailsToFirestore()
      }).catchError((e)
      {
        Fluttertoast.showToast(msg: e!.message);
      });

    }
  }

  postDetailsToFirestore() async {
    FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;
    User? user = _auth.currentUser;

    UserModel userModel = UserModel();
    userModel.email = user!.email;
    userModel.uid = user.uid;

    await firebaseFirestore
    .collection("users")
    .doc(user.uid)
    .set(userModel.toMap());
    Navigator.pushAndRemoveUntil((context), MaterialPageRoute(builder: (context) => RootPage()), (route) => false);
  }
}
