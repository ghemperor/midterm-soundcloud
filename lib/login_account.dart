import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cupertino_icons/cupertino_icons.dart';
import 'package:flutter/services.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:email_validator/email_validator.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mysoundcloud/root_screen.dart';
import 'home_screen.dart';
Color btn_back = Color(0xFF303030);
Color crtacc = Color(0xFFdedede);
Color google = Color(0xFF303030);

class LoginUI extends StatefulWidget {
  const LoginUI({Key? key}) : super(key: key);

  @override
  State<LoginUI> createState() => _LoginUIState();
}

class _LoginUIState extends State<LoginUI> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController emailController = new TextEditingController();
  final TextEditingController passwordController = new TextEditingController();

  // Firebase
  final _auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    final emailField = TextFormField(
      style: TextStyle(
          color: Color(0xFF979797)
      ),
      decoration: const InputDecoration(
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Color(0xFF979797)),
        ),
        hintText: "Email",
        hintStyle: TextStyle(
            color: Color(0xFF979797)
        ),
      ),
      autofocus: false,
      controller: emailController,
      keyboardType: TextInputType.emailAddress,
      validator: (value) {
        if(value!.isEmpty) {
          return "Please enter your email!";
        }
        if(EmailValidator.validate(value) == false) {
          return "Please enter valid email!";
        }
        return null;
      },
      onSaved: (value)
      {
        emailController.text = value!;
      },
      textInputAction: TextInputAction.next,
    );

    final passwordField = TextFormField(
      obscureText: true,
      validator: (value) {
        if(value!.isEmpty) {
          return "Please enter password!";
        }
        return null;
      },
      style: TextStyle(
          color: Color(0xFF979797)
      ),
      decoration: const InputDecoration(
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Color(0xFF979797)),
        ),
        hintText: "Password",
        hintStyle: TextStyle(
            color: Color(0xFF979797)
        ),
      ),
      autofocus: false,
      controller: passwordController,
      onSaved: (value)
      {
        passwordController.text = value!;
      },
      textInputAction: TextInputAction.done,
    );

    final loginButton = TextButton(
        style: TextButton.styleFrom(
            shadowColor: Colors.black,
            primary: Colors.black,
            backgroundColor: Color(0xFF666666)
        ),
        onPressed: () {
          signIn(emailController.text , passwordController.text);

        },
        child: Text('Continue'));
    return Container(
      child: GestureDetector(
          onTap: () {
            FocusManager.instance.primaryFocus?.unfocus();
          },
          child: Scaffold(
            backgroundColor: Colors.black,
            body: ListView(
                children:[
                  Column(
                    children: <Widget>[
                      Align(
                        alignment: Alignment.topCenter,
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(7, 30, 0, 0),
                          child: Row(
                            children: <Widget>[
                              ElevatedButton(
                                onPressed: () {
                                  Navigator.pop(context);

                                },
                                child: Icon(CupertinoIcons.arrow_left, color: Colors.white),
                                style: ElevatedButton.styleFrom(
                                  shape: CircleBorder(),
                                  primary: btn_back,
                                  fixedSize: Size(40, 40),
                                ),
                              ),
                              Text(
                                " Sign in",
                                style: TextStyle(
                                  color: crtacc,
                                  fontSize: 25,
                                  fontWeight: FontWeight.w500,
                                  letterSpacing: 0.4,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 30),
                        child: SizedBox(
                          width: 370,
                          height: 45,
                          child:  SignInButton(
                            Buttons.Google,
                            text: "Continue with Google",
                            onPressed: () {},
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: SizedBox(
                          width: 370,
                          height: 45,
                          child:  SignInButton(
                            Buttons.Facebook,
                            text: "Continue with Facebook",
                            onPressed: () {},
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: SizedBox(
                          width: 370,
                          height: 45,
                          child:  SignInButton(
                            Buttons.Apple,
                            text: "Continue with Apple",
                            onPressed: () {},
                          ),
                        ),
                      ),
                      Container(
                        child: Form(
                          key: _formKey,
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(top: 20),
                                child: SizedBox(
                                  width: MediaQuery.of(context).size.width * 0.9,
                                  height: MediaQuery.of(context).size.height * 0.07,
                                  child: emailField,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 10),
                                child: SizedBox(
                                  width: MediaQuery.of(context).size.width * 0.9,
                                  height: MediaQuery.of(context).size.height * 0.07,
                                  child: passwordField,
                                ),
                              ),

                            ],
                          ),

                        ),
                      ),
                      // Padding(
                      //     padding: EdgeInsets.only(top: 20),
                      //     child: SizedBox(
                      //         width: MediaQuery.of(context).size.width * 0.9,
                      //         height: MediaQuery.of(context).size.height * 0.07,
                      //         child:  TextFormField(
                      //           validator: (value) => EmailValidator.validate(value) ? null : "Please enter a valid email",
                      //           style: TextStyle(
                      //               color: Color(0xFF979797)
                      //           ),
                      //           decoration: const InputDecoration(
                      //
                      //             enabledBorder: UnderlineInputBorder(
                      //               borderSide: BorderSide(color: Color(0xFF979797)),
                      //             ),
                      //             hintText: "Email",
                      //             hintStyle: TextStyle(
                      //                 color: Color(0xFF979797)
                      //             ),
                      //
                      //           ),
                      //         )
                      //     )
                      // ),
                      // Padding(
                      //     padding: EdgeInsets.only(top: 20),
                      //     child: SizedBox(
                      //         width: MediaQuery.of(context).size.width * 0.9,
                      //         height: MediaQuery.of(context).size.height * 0.07,
                      //         child: TextFormField(
                      //           validator: (value) {
                      //             if(value==null) {
                      //               return "Enter your password";
                      //             }
                      //             return null;
                      //           },
                      //           obscureText: true,
                      //           style: TextStyle(
                      //               color: Color(0xFF979797)
                      //           ),
                      //           decoration: const InputDecoration(
                      //             enabledBorder: UnderlineInputBorder(
                      //               borderSide: BorderSide(color: Color(0xFF979797)),
                      //             ),
                      //             hintText: "Password (min. 8 characters)",
                      //             hintStyle: TextStyle(
                      //                 color: Color(0xFF979797)
                      //             ),
                      //           ),
                      //         )
                      //     )
                      // ),
                      Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: SizedBox (
                            width: MediaQuery.of(context).size.width * 0.9,
                            height: MediaQuery.of(context).size.height * 0.07,
                            child: loginButton
                        ),),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(22, 20, 0, 0),
                          child: Text(
                            "Forgot your password?",
                            style: TextStyle(
                              color: Color(0xFF5885d6),
                              fontSize: 16,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ]),
          )),
    );
  }
  void signIn(String email, String password) async {
    if(_formKey.currentState!.validate()) {
      await _auth
          .signInWithEmailAndPassword(email: email, password: password)
          .then((uid) => {
            Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => RootPage())),
      }).catchError((e){
        Fluttertoast.showToast(msg: e!.message);

      });
    }
  }
}


